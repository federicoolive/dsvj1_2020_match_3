#ifndef RUNAS_H
#define RUNAS_H

#include "raylib.h"
#include <time.h>
#include "configuraciones/estructuras.h"
#include "configuraciones/constantes.h"
#include "escenario/escenario.h"
#include "juego/juego.h"
#include "animaciones/animaciones.h"

namespace match
{
	namespace runas
	{
		const int maxFrames = 11;

		struct TEXTURAS
		{
			Texture2D tx;
		};
		extern TEXTURAS texturas[static_cast<int>(TIPO::LIMITE)];

		struct COMPONENTESBLOQUE
		{
			TIPO tipo = TIPO::LIMITE;
			Rectangle sourceRec = { 0, 0, 0, 0 };

			Color color = WHITE;
		};

		struct BLOQUE
		{
			int id;
			bool activo = true;
			bool enDestruccion = false;
			bool enSeleccion = false;
			Rectangle rec = { 0 };
			COMPONENTESBLOQUE stat;
		};
		extern BLOQUE bloque[tamCol][tamFil];

		void init();
		void deinit();
		void setearTipo(int x, int y);
		void draw();
		void primerSeleccion(bool& enSeleccion);
		void multipleSeleccion();
		void deSeleccionar();
		void destruccion();
		void bajarRunas();
		void reiniciarPosiciones();
		void reordenarRunas();
		void reaparecerBloques();
	}
}

#endif