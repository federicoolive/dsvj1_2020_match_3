#include "juego.h"
#include <time.h>
#include "escenario/escenario.h"
#include "marcos/marco.h"
#include "origen/loop.h"
#include "configuraciones/configuraciones.h"
#include "runas.h"
#include "jugador/jugador.h"
#include "menu/menues.h"
#include "audio/audio.h"

using namespace std;

namespace match
{
	namespace juego
	{
		int nivelActual = 0;
		int maxScore = 100 + nivelActual * (100 * 0.2);
		int turnosActuales = 0;
		int score = 0;
		bool gameOver = false;
		bool enPausa = false;
		bool hayAtaque = false;
		BOTON panel;
		BOTON headerPausa;
		BOTON headerGameOver;
		BOTON btnRecargarGrilla;
		BOTON btnPausa;
		BOTON btnCerrar;
		BOTON reiniciar;
		BOTON alMenu;

		bool enSeleccion;
		Vector2 posBloquesDestruir[tamFil * tamCol] = { 0 };
		int cantBloquesIndex = -1;

		int clockAnim = 0;
		bool iniciarAnimacionDestruccion = false;
		bool animTerminada = false;
		bool reconstruccionNuevoBloque = false;

		Vector2 mouse;

		int ultimoSeleccionado;
		Vector2 posicionesSeleccionadas[tamCol * tamFil];

		void init()
		{
			nivelActual = 0;
			maxScore = 100 * (nivelActual + 1) + nivelActual * 10;
			turnosActuales = 0;
			score = 0;

			turnosActuales = 0;
			score = 0;
			gameOver = false;
			enPausa = false;
			hayAtaque = false;
			escenario::init();
			runas::init();

			btnRecargarGrilla.textura = LoadTexture("res/assets/texturas/recargar.png");
			btnRecargarGrilla.textura.width = ancho * config::escalado;
			btnRecargarGrilla.textura.height = alto * config::escalado;
			btnRecargarGrilla.rec.width = btnRecargarGrilla.textura.width;
			btnRecargarGrilla.rec.height = btnRecargarGrilla.textura.height;
			btnRecargarGrilla.rec.x = escenario::panelMatch.x + escenario::panelMatch.width / 2 - btnRecargarGrilla.rec.width / 2;
			btnRecargarGrilla.rec.y = escenario::panelMatch.y - btnRecargarGrilla.rec.height;
			btnRecargarGrilla.color = WHITE;

			btnPausa.textura = LoadTexture("res/assets/texturas/pause.png");
			btnPausa.textura.width = ancho * config::escalado;
			btnPausa.textura.height = alto * config::escalado;
			btnPausa.rec.width = btnPausa.textura.width;
			btnPausa.rec.height = btnPausa.textura.height;
			btnPausa.rec.x = GetScreenWidth() - btnRecargarGrilla.rec.width - OFFSET;
			btnPausa.rec.y = OFFSET;
			btnPausa.color = WHITE;

			reiniciar.textura = btnRecargarGrilla.textura;
			reiniciar.textura.width = ancho * config::escalado * 2;
			reiniciar.textura.height = alto * config::escalado * 2;
			reiniciar.rec.width = reiniciar.textura.width;
			reiniciar.rec.height = reiniciar.textura.height;
			reiniciar.rec.x = escenario::panelPausa.x + escenario::panelPausa.width / 2 - reiniciar.rec.width / 2;
			reiniciar.rec.y = escenario::panelPausa.y + escenario::panelPausa.height * 2 / 10;
			reiniciar.color = WHITE;

			alMenu.textura = LoadTexture("res/assets/texturas/menu.png");
			alMenu.textura.width = ancho * config::escalado * 2;
			alMenu.textura.height = alto * config::escalado * 2;
			alMenu.rec.width = alMenu.textura.width;
			alMenu.rec.height = alMenu.textura.height;
			alMenu.rec.x = escenario::panelPausa.x + escenario::panelPausa.width / 2 - alMenu.rec.width / 2;
			alMenu.rec.y = escenario::panelPausa.y + escenario::panelPausa.height * 6 / 10;
			alMenu.color = WHITE;

			panel.textura = LoadTexture("res/assets/texturas/panel_pausa.png");
			panel.textura.width = escenario::panelPausa.width;
			panel.textura.height = escenario::panelPausa.height;
			panel.rec.width = panel.textura.width;
			panel.rec.height = panel.textura.height;
			panel.rec.x = escenario::panelPausa.x;
			panel.rec.y = escenario::panelPausa.y;
			panel.color = WHITE;

			btnCerrar.textura = LoadTexture("res/assets/texturas/cerrar.png");
			btnCerrar.textura.width = ancho * config::escalado * 2;
			btnCerrar.textura.height = alto * config::escalado * 2;
			btnCerrar.rec.width = btnCerrar.textura.width;
			btnCerrar.rec.height = btnCerrar.textura.height;
			btnCerrar.rec.x = panel.rec.x + panel.rec.width - btnCerrar.rec.width / 2;
			btnCerrar.rec.y = panel.rec.y - btnCerrar.rec.height / 2;
			btnCerrar.color = WHITE;

			headerPausa.textura = LoadTexture("res/assets/texturas/header_pausa.png");
			headerPausa.textura.width = panel.textura.width * config::escalado;
			headerPausa.textura.height = panel.textura.width * 0.39481f * config::escalado;
			headerPausa.rec.width = headerPausa.textura.width;
			headerPausa.rec.height = headerPausa.textura.height;
			headerPausa.rec.x = panel.rec.x ;
			headerPausa.rec.y = panel.rec.y - headerPausa.rec.height / 2;
			headerPausa.color = WHITE;

			headerGameOver.textura = LoadTexture("res/assets/texturas/header_gameover.png");
			headerGameOver.textura.width = panel.textura.width * config::escalado;
			headerGameOver.textura.height = panel.textura.width * 0.39481f * config::escalado;
			headerGameOver.rec.width = headerGameOver.textura.width;
			headerGameOver.rec.height = headerGameOver.textura.height;
			headerGameOver.rec.x = panel.rec.x;
			headerGameOver.rec.y = panel.rec.y - headerGameOver.rec.height / 2;
			headerGameOver.color = WHITE;
		}

		void play()
		{
			mouse = GetMousePosition();
			if (!enPausa && !gameOver)
			{
				animaciones::animacionesManagerRunas();
				animaciones::animacionesManagerPersonaje();
				if (animaciones::estadoAnim == animaciones::ESTADOANIM::NINGUNO && !gameOver)
				{
					input();
					inputPausa();
					update();
				}
			}
			else if (enPausa || gameOver)
			{
				inputPausa();
			}
			draw();
		}

		void update()
		{
			if (ultimoSeleccionado > 0)
			{
				cout << "Posiciones seleccionadas: " << endl;
				for (int i = 0; i <= ultimoSeleccionado; i++)
				{
					cout << "\t" << posicionesSeleccionadas[i].x << " - " << posicionesSeleccionadas[i].y << endl;
				}
			}

			if (score > maxScore)
			{
				nivelActual++;
				maxScore = 100 * (nivelActual + 1) + nivelActual * 10;
				turnosActuales = 0;
			}

			if (turnosActuales > maxTurnos)
			{
				gameOver = true;
				nivelActual = 0;
				maxScore = 100;
				turnosActuales = 0;
			}
		}

		void input()
		{
			runas::deSeleccionar();

			if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
			{
				if (CheckCollisionPointRec(mouse, escenario::panelMatch))
				{
					std::cout << "Push"<< std::endl;
					runas::primerSeleccion(enSeleccion);
				}
				else if (CheckCollisionPointRec(mouse, btnRecargarGrilla.rec))
				{
					juego::turnosActuales++;
					for (int i = 0; i < tamCol; i++)
					{
						for (int j = 0; j < tamFil; j++)
						{
							runas::bloque[i][j].activo = true;
							runas::bloque[i][j].enDestruccion = false;
							runas::bloque[i][j].enSeleccion = false;
							runas::bloque[i][j].stat.color = WHITE;
							runas::setearTipo(i, j);
						}
					}
				}
			}
			if (IsMouseButtonDown(MOUSE_LEFT_BUTTON) && enSeleccion)
			{
				runas::multipleSeleccion();
			}
		}
		
		void inputPausa()
		{
			if (enPausa || gameOver)
			{
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					if (CheckCollisionPointRec(mouse, reiniciar.rec))
					{
						deinit();
						init();
						audio::musica[1].activo = false;
						audio::musica[2].activo = true;
					}
					else if (CheckCollisionPointRec(mouse, alMenu.rec))
					{
						deinit();
						menues::init();
						menues::menuActual = menues::MENU::PRINCIPAL;
						audio::musica[1].activo = false;
						audio::musica[2].activo = false;
						audio::musica[0].activo = true;
					}
					else if (CheckCollisionPointRec(mouse, btnCerrar.rec)&&(!gameOver))
					{
						enPausa = !enPausa;
					}
				}
			}
			else
			{
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					if (CheckCollisionPointRec(mouse, btnPausa.rec))
					{
						enPausa = !enPausa;
					}
				}
			}
		}

		void draw()
		{
			BeginDrawing();
			ClearBackground(WHITE);

			escenario::draw();

			DrawTextureEx(btnRecargarGrilla.textura, { btnRecargarGrilla.rec.x, btnRecargarGrilla.rec.y }, 0, 1, btnRecargarGrilla.color);
			DrawTextureEx(btnPausa.textura, { btnPausa.rec.x, btnPausa.rec.y }, 0, 1, btnPausa.color);

			runas::draw();

			marco::draw();

			DrawText(TextFormat("Score: %i", score), GetScreenWidth() / 2 - MeasureText(TextFormat("Score %i", score), 15) / 2, GetScreenHeight() * 1 / 5, 15, BLACK);
			if (maxTurnos - turnosActuales > 0)
				DrawText(TextFormat("Turnos Restantes: %i", maxTurnos - turnosActuales), GetScreenWidth() / 2 - MeasureText(TextFormat("Turnos Restantes: %i", maxTurnos - turnosActuales), 15) / 2, GetScreenHeight() * 1 / 5 - 30, 15, BLACK);
			else
				DrawText(TextFormat("Turnos Restantes: %i", 0), GetScreenWidth() / 2 - MeasureText(TextFormat("Turnos Restantes: %i", 0), 15) / 2, GetScreenHeight() * 1 / 5 - 30, 15, BLACK);

			DrawText(TextFormat("Alcanzar: %i", maxScore), GetScreenWidth() / 2 - MeasureText(TextFormat("Alcanzar: %i", maxScore), 15) / 2, GetScreenHeight() * 1 / 5 - 45, 15, BLACK);
			DrawText(TextFormat("Nivel Actual: %i", nivelActual), GetScreenWidth() / 2 - MeasureText(TextFormat("Nivel Actual: %i", nivelActual), 15) / 2, GetScreenHeight() * 1 / 5 - 60, 15, BLACK);

			if (enPausa || gameOver)
			{
				DrawTextureEx(panel.textura, { panel.rec.x, panel.rec.y }, 0, 1, panel.color);
				DrawTextureEx(reiniciar.textura, { reiniciar.rec.x, reiniciar.rec.y }, 0, 1, reiniciar.color);
				DrawTextureEx(alMenu.textura, { alMenu.rec.x, alMenu.rec.y }, 0, 1, alMenu.color);
				if (!gameOver)
				{
					DrawTextureEx(headerPausa.textura, { headerPausa.rec.x, headerPausa.rec.y }, 0, 1, headerPausa.color);
					DrawTextureEx(btnCerrar.textura, { btnCerrar.rec.x, btnCerrar.rec.y }, 0, 1, btnCerrar.color);
				}
				else
				{
					DrawTextureEx(headerGameOver.textura, { headerGameOver.rec.x, headerGameOver.rec.y }, 0, 1, headerGameOver.color);
				}
			}

			EndDrawing();
		}

		void deinit()
		{
			escenario::deinit();
			runas::deinit();
			UnloadTexture(btnRecargarGrilla.textura);
			UnloadTexture(btnPausa.textura);
			UnloadTexture(reiniciar.textura);
			UnloadTexture(alMenu.textura);
			UnloadTexture(panel.textura);
			UnloadTexture(btnCerrar.textura);
		}				  
	}
}