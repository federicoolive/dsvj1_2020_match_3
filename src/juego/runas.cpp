#include "runas.h"
#include "configuraciones/configuraciones.h"

namespace match
{
	namespace runas
	{
		BLOQUE bloque[tamCol][tamFil];
		TEXTURAS texturas[static_cast<int>(TIPO::LIMITE)];

		void init()
		{
			texturas[static_cast<int>(TIPO::AGUA)].tx = LoadTexture("res/assets/texturas/runa_ataque.png");
			texturas[static_cast<int>(TIPO::ESCUDO)].tx = LoadTexture("res/assets/texturas/runa_escudo.png");
			texturas[static_cast<int>(TIPO::FUEGO)].tx = LoadTexture("res/assets/texturas/runa_fuego.png");
			texturas[static_cast<int>(TIPO::TIERRA)].tx = LoadTexture("res/assets/texturas/runa_tierra.png");
			texturas[static_cast<int>(TIPO::TRUENO)].tx = LoadTexture("res/assets/texturas/runa_trueno.png");
			texturas[static_cast<int>(TIPO::VIDA)].tx = LoadTexture("res/assets/texturas/runa_vida.png");

			for (int i = 0; i < static_cast<int>(TIPO::LIMITE); i++)
			{
				texturas[i].tx.width = texturas[i].tx.width * 0.3 * config::escalado;
				texturas[i].tx.height = texturas[i].tx.height * 0.3 * config::escalado;
			}
			
			Vector2 pos = { escenario::panelMatch.x, escenario::panelMatch.y };

			for (int i = 0; i < tamCol; i++)
			{
				pos.y = escenario::panelMatch.y;
				for (int j = 0; j < tamFil; j++)
				{
					bloque[i][j].id = j + tamFil * i;
					bloque[i][j].activo = true;
					bloque[i][j].rec = { pos.x, pos.y, ancho, alto };
					bloque[i][j].stat.sourceRec = { 0, 0, static_cast<float>(texturas[0].tx.width / maxFrames), static_cast<float>(texturas[0].tx.height) };
					setearTipo(i, j);
					pos.y += alto * config::escalado;
				}
				pos.x += ancho * config::escalado;
			}
		}

		void deinit()
		{
			for (int i = 0; i < static_cast<int>(TIPO::LIMITE); i++)
			{
				UnloadTexture(texturas[i].tx);
			}
		}
		void setearTipo(int x, int y)
		{
			int random = GetRandomValue(static_cast<int>(TIPO::FUEGO), static_cast<int>(TIPO::LIMITE) - 1);
			bloque[x][y].stat.tipo = static_cast<TIPO>(random);
		}
		void draw()
		{
			for (int i = 0; i < tamCol; i++)
			{
				for (int j = 0; j < tamFil; j++)
				{
					if (bloque[i][j].activo)
					{
						DrawTextureRec(texturas[static_cast<int>(bloque[i][j].stat.tipo)].tx, bloque[i][j].stat.sourceRec, { bloque[i][j].rec.x, bloque[i][j].rec.y }, bloque[i][j].stat.color);
					}
				}
			}
		}

		void primerSeleccion(bool& enSeleccion)
		{
			juego::ultimoSeleccionado = 0;
			for (int i = 0; i < tamCol; i++)
			{
				for (int j = 0; j < tamFil; j++)
				{
					if (CheckCollisionPointRec(juego::mouse, bloque[i][j].rec))
					{
						bloque[i][j].enSeleccion = true;
						bloque[i][j].stat.color = GRAY;
						juego::enSeleccion = true;

						juego::posicionesSeleccionadas[juego::ultimoSeleccionado] = { static_cast<float>(i), static_cast<float>(j) };

						std::cout << "Seleccionado: " << i << " - " << j << std::endl;
						std::cout << "TIPO: " << static_cast<int>(bloque[i][j].stat.tipo) << std::endl;
						std::cout << "Seleccionado: " << bloque[i][j].id << std::endl;

					}
				}
			}
		}

		void multipleSeleccion()
		{
			for (int i = 0; i < tamCol; i++)
			{
				for (int j = 0; j < tamFil; j++)
				{
					if (CheckCollisionPointRec(juego::mouse, bloque[i][j].rec))
					{
						// Si es de igual tipo
						if (bloque[i][j].stat.tipo == bloque[static_cast<int>(juego::posicionesSeleccionadas[juego::ultimoSeleccionado].x)][static_cast<int>(juego::posicionesSeleccionadas[juego::ultimoSeleccionado].y)].stat.tipo)
						{
							// Si el ID del ultimo seleccionado es diferente al de donde tiene el mouse ahora
							if (bloque[static_cast<int>(juego::posicionesSeleccionadas[juego::ultimoSeleccionado].x)][static_cast<int>(juego::posicionesSeleccionadas[juego::ultimoSeleccionado].y)].id != bloque[i][j].id)
							{
								// Si el ultimo seleccionado est� a 1 bloque de dist de donde tiene mouse (Derecha ^ Izquierda ^ Arriba ^ Abajo)
								int horizontal = (juego::posicionesSeleccionadas[juego::ultimoSeleccionado].x - i);
								int vertical = (juego::posicionesSeleccionadas[juego::ultimoSeleccionado].y - j);

								if ((abs(horizontal) == 1 && vertical == 0) || (abs(vertical) == 1 && horizontal == 0))
								{
									if (bloque[i][j].enSeleccion)
									{
										bloque[static_cast<int>(juego::posicionesSeleccionadas[juego::ultimoSeleccionado].x)][static_cast<int>(juego::posicionesSeleccionadas[juego::ultimoSeleccionado].y)].enSeleccion = false;
										juego::posicionesSeleccionadas[juego::ultimoSeleccionado] = { 0, 0 };
										juego::ultimoSeleccionado--;
									}
									else
									{
										juego::ultimoSeleccionado++;
										juego::posicionesSeleccionadas[juego::ultimoSeleccionado] = { static_cast<float>(i), static_cast<float>(j) };
										bloque[i][j].enSeleccion = true;
										bloque[i][j].stat.color = GRAY;
									}
									std::cout << "Seleccionados: " << juego::ultimoSeleccionado << std::endl;
									for (int i = 0; i < 5; i++)
									{
										std::cout << "Posiciones: " << juego::posicionesSeleccionadas[i].x << " - " << juego::posicionesSeleccionadas[i].y << std::endl;
									}
								}
								else
								{
									bool retroceso = false;
									for (int k = 0; k < tamCol * tamFil; k++)
									{
										if (juego::posicionesSeleccionadas[k].x == i && juego::posicionesSeleccionadas[k].y == j && !retroceso)
										{
											if (juego::ultimoSeleccionado != 0)
											{
												juego::ultimoSeleccionado = k;
											}
											else
											{
												juego::ultimoSeleccionado = 0;
											}
											
											retroceso = true;
										}
										else if (retroceso)
										{
											juego::posicionesSeleccionadas[k] = { 0, 0 };
										}
									}
								}
							}
						}
					}
				}
			}
		}

		void deSeleccionar()
		{
			for (int i = 0; i < tamCol; i++)
			{
				for (int j = 0; j < tamFil; j++)
				{
					bloque[i][j].stat.color = WHITE;
					bloque[i][j].enSeleccion = false;
				}
			}
			if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
			{
				if (CheckCollisionPointRec(juego::mouse, escenario::panelMatch) && juego::enSeleccion)
				{
					for (int i = 0; i <= juego::ultimoSeleccionado; i++)
					{
						bloque[static_cast<int>(juego::posicionesSeleccionadas[i].x)][static_cast<int>(juego::posicionesSeleccionadas[i].y)].stat.color = GRAY;
						bloque[static_cast<int>(juego::posicionesSeleccionadas[i].x)][static_cast<int>(juego::posicionesSeleccionadas[i].y)].enSeleccion = true;
					}
				}
			}
			else if (IsMouseButtonUp(MOUSE_LEFT_BUTTON))
			{
				juego::enSeleccion = false;
				juego::ultimoSeleccionado = 0;
				juego::posicionesSeleccionadas[0] = { 0, 0 };
			}
		}

		void destruccion()
		{
			for (int i = 0; i <= juego::ultimoSeleccionado; i++)
			{
				Vecint2 pos = { static_cast<int>(juego::posicionesSeleccionadas[i].x), static_cast<int>(juego::posicionesSeleccionadas[i].y) };

				if (bloque[pos.x][pos.y].stat.sourceRec.x < (texturas[0].tx.width - bloque[pos.x][pos.y].stat.sourceRec.width * 1.5))
				{
					bloque[pos.x][pos.y].stat.sourceRec.x += bloque[pos.x][pos.y].stat.sourceRec.width;
				}
				else
				{
					bloque[pos.x][pos.y].activo = false;
					animaciones::estadoAnim = animaciones::ESTADOANIM::ORDENAMIENTO;
				}
			}			
		}

		void bajarRunas()
		{
			bool hayOrdenamiento = false;
			for (int i = 0; i < tamCol; i++)
			{
				bool bajar = false;
				Vecint2 bloqueInferior = { -1, -1 };

				for (int j = tamFil-1; j >= 0; j--)
				{
					if (!bloque[i][j].activo)
					{
						if (!bajar)
						{
							bloqueInferior = { i, j + 1 };
						}
						bajar = true;
					}
					else if (bloque[i][j].activo && bajar)
					{
						if (!CheckCollisionRecs(bloque[i][j].rec, bloque[bloqueInferior.x][bloqueInferior.y].rec) && bloque[i][j].rec.y + bloque[i][j].rec.height < escenario::panelMatch.y + escenario::panelMatch.height)
						{
							bloque[i][j].rec.y += 2;
							hayOrdenamiento = true;
						}
						else
						{
							bajar = false;
						}
					}
				}
			}
			if (!hayOrdenamiento)
			{
				reiniciarPosiciones();
				reordenarRunas();
				animaciones::estadoAnim = animaciones::ESTADOANIM::APARICION;
			}
		}

		void reiniciarPosiciones()
		{
			Vector2 pos = { escenario::panelMatch.x, escenario::panelMatch.y };
			for (int i = 0; i < tamCol; i++)
			{
				pos.y = escenario::panelMatch.y;
				for (int j = 0; j < tamFil; j++)
				{
					bloque[i][j].rec = { pos.x, pos.y, ancho, alto };
					pos.y += alto;
				}
				pos.x += ancho;
			}
		}
		
		void reordenarRunas()
		{
			for (int i = 0; i < tamCol; i++)
			{
				int bajarEnColumnaActual = 0;
				for (int k = 0; k <= juego::ultimoSeleccionado; k++)
				{
					if (juego::posicionesSeleccionadas[k].x == i)
					{
						bajarEnColumnaActual++;
					}
				}

				for (int j = tamFil - 1; j >= 0; j--)
				{
					if (!bloque[i][j].activo && j - bajarEnColumnaActual>=0)
					{
						bloque[i][j] = bloque[i][j - bajarEnColumnaActual];
						bloque[i][j - bajarEnColumnaActual].activo = false;

					}
				}
			}
			reiniciarPosiciones();
		}

		void reaparecerBloques()
		{
			for (int i = 0; i < tamCol; i++)
			{
				for (int j = 0; j < tamFil; j++)
				{
					if (!bloque[i][j].activo)
					{
						bloque[i][j].id = j + tamFil * i;
						bloque[i][j].activo = true;
						bloque[i][j].stat.sourceRec = { 0, 0, static_cast<float>(texturas[0].tx.width / maxFrames), static_cast<float>(texturas[0].tx.height) };
						bloque[i][j].stat.sourceRec.x = bloque[i][j].stat.sourceRec.width * (maxFrames - 1);
						setearTipo(i, j);
					}
				}
			}

			for (int i = 0; i < tamCol; i++)
			{
				for (int j = 0; j < tamFil; j++)
				{
					if (bloque[i][j].stat.sourceRec.x != 0)
					{
						bloque[i][j].stat.sourceRec.x -= bloque[i][j].stat.sourceRec.width;
						setearTipo(i, j);
					}
					bloque[i][j].id = j + tamFil * i;
				}
			}

			bool animTerminada = true;
			for (int i = 0; i < tamCol; i++)
			{
				for (int j = 0; j < tamFil; j++)
				{
					if (bloque[i][j].stat.sourceRec.x != 0)
					{
						animTerminada = false;
						break;
					}
				}
			}
			if (animTerminada)
			{
				animaciones::estadoAnim = animaciones::ESTADOANIM::NINGUNO;
			}
		}
	}
}