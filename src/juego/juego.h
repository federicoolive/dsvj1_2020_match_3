#ifndef JUEGO_H
#define JUEGO_H
#include <iostream>
#include "raylib.h"
#include "configuraciones/estructuras.h"
#include "configuraciones/constantes.h"

namespace match
{
	namespace juego
	{
		extern int maxScore;
		extern int nivelActual;
		extern int turnosActuales;
		extern int score;
		extern bool gameOver;
		extern bool enPausa;
		extern BOTON btnRecargarGrilla;
		extern BOTON btnPausa;
		extern BOTON reiniciar;
		extern BOTON alMenu;
		extern bool enSeleccion;
		extern int clockJuego;

		extern Vector2 mouse;
		extern int ultimoSeleccionado;
		extern Vector2 posicionesSeleccionadas[tamCol * tamFil];
		void init();
		void play();
		void update();
		void input();
		void inputPausa();
		void draw();
		void deinit();
	}
}

#endif