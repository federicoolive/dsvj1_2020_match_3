#include "loop.h"
#include <iostream>
#include <time.h>
#include "textos/texto.h"
#include "configuraciones/configuraciones.h"
#include "menu/menues.h"
#include "juego/juego.h"
#include "audio/audio.h"

namespace match
{
	namespace loop
	{
		int clockUpdate;

		void juego()
		{
			txt::actualizarLenguaje();
			config::init();

			InitAudioDevice();
			audio::init();

			menues::menuActual = menues::MENU::PRINCIPAL;
			audio::musica[0].activo = true;

			while (!WindowShouldClose() && config::enJuego)
			{
				if (IsKeyPressed(KEY_M))	// muteo musica
				{
					audio::musicas = !audio::musicas; 
				}
				if (IsKeyPressed(KEY_N))	// muteo efectos
				{
					audio::efectos = !audio::efectos;
				}
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					std::cout << GetMousePosition().y << std::endl;
				}
				audio::update();
				clockUpdate = clock();
				switch (menues::menuActual)
				{
				case menues::MENU::PRINCIPAL:

					menues::principal::update();

					break;
				case menues::MENU::JUEGO:

					juego::play();

					break;
				case menues::MENU::CREDITOS:

					menues::creditos::update();

					break;
				default:
					break;
				}
			}
			config::deInit();
			CloseWindow();

		}
	}
}