#ifndef CONSTANTES_H
#define CONSTANTES_H

#include "raylib.h"

namespace match
{
	namespace constantes
	{
		enum class TIPO { FUEGO, AGUA, TIERRA, TRUENO, VIDA, ESCUDO, LIMITE };

		const int tamFil = 10;
		const int tamCol = 7;
		const int maxBloques = tamFil * tamCol;
		const int minCombo = 3;

		//------ Tiempos -----
		const int tiempoDestruccion = 25;
		const int tiempoAparicion = 50;

		const int maxTurnos = 7;

		//------ Runas de Grilla -----
		const int ancho = 30;
		const int alto = 30;
	}

	using namespace constantes;
}

#endif