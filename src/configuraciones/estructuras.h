#ifndef ESTRUCTURAS_H
#define ESTRUCTURAS_H

#include "raylib.h"
namespace match
{
	namespace stt
	{
		struct BOTON
		{
			Rectangle rec;
			Texture2D textura;
			Color color = WHITE;
		};

		struct Vecint2
		{
			int x = 0;
			int y = 0;
		};
	}

	using namespace stt;
}

#endif