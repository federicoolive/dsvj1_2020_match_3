#ifndef CONFIGURACIONES_H
#define CONFIGURACIONES_H

#include "raylib.h"

namespace match
{
	namespace config
	{
		extern Vector2 pantallaInicial[5];
		extern int resolActual;
		extern int fpsTarget;
		
		extern bool enJuego;
		extern float escalado;
		extern int minMatch;

		void init();
		void update();
		void deInit();
	}
}

#endif