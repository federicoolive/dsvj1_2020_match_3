#include "configuraciones.h"
#include "escenario/escenario.h"
#include "marcos/marco.h"
#include "menu/menues.h"
#include "juego/juego.h"
#include "audio/audio.h"
#include "textos/texto.h"

namespace match
{
	namespace config
	{
		Vector2 pantallaInicial[5];
		int resolActual;

		bool enJuego;
		int fpsTarget = 60;
		float escalado;
		int minMatch = 3;

		Vector2 posCentro;

		void init()
		{
			pantallaInicial[0] = { 338,  600 };
			pantallaInicial[1] = { 394,  700 };
			pantallaInicial[2] = { 450,  800 };
			pantallaInicial[3] = { 506,  900 };
			pantallaInicial[4] = { 563, 1000 };
			enJuego = true;
			resolActual = 0;

			InitWindow(pantallaInicial[resolActual].x, pantallaInicial[resolActual].y, &txt::titulo[0]);
			posCentro.x = GetWindowPosition().x + pantallaInicial[resolActual].x / 2;
			posCentro.y = GetWindowPosition().y + pantallaInicial[resolActual].y / 2;

			SetTargetFPS(fpsTarget);
			menues::menuInmersion = 0;
			update();
		}

		void update()
		{
			SetWindowSize(pantallaInicial[resolActual].x, pantallaInicial[resolActual].y);
			SetWindowPosition(posCentro.x - pantallaInicial[resolActual].x / 2, posCentro.y - pantallaInicial[resolActual].y / 2);

			escalado = pantallaInicial[resolActual].x / pantallaInicial[0].x;

			//------ Inits ------
			menues::init();
		}

		void deInit()
		{
			audio::deinit();
		}
	}
}