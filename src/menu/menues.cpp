#include "menues.h"
#include <iostream>
#include <time.h>
#include "configuraciones/estructuras.h"
#include "configuraciones/configuraciones.h"
#include "escenario/escenario.h"
#include "marcos/marco.h"
#include "audio/audio.h"
#include "textos/texto.h"
#include "juego/juego.h"

namespace match
{
	namespace menues
	{
		MENU menuActual;
		int menuInmersion = 0;
		Vector2 mouse = { 0 };
		Texture2D fondo;

		void init()
		{
			principal::init();
		}

		namespace principal
		{
			BOTON btnJugar;
			BOTON btnOpciones;
			BOTON btnCreditos;
			BOTON btnMusica;
			BOTON btnEfectos;
			BOTON btnSalir;

			void init()
			{
				marco::init();
				// ----- Carga de Texturas -----
				btnCreditos.textura = LoadTexture("res/assets/menu/about.png");
				fondo = LoadTexture("res/assets/menu/bg.png");
				btnJugar.textura = LoadTexture("res/assets/menu/play.png");
				btnOpciones.textura = LoadTexture("res/assets/menu/setting.png");
				
				actualizarTexturaAudio();

				btnSalir.textura = LoadTexture("res/assets/menu/exit.png");

				// ----- Tama�o de Texturas -----
				fondo.width = GetScreenWidth();
				fondo.height = GetScreenHeight();

				btnJugar.textura.width = btnJugar.rec.width = BOTON_TAM * 5 * config::escalado;
				btnJugar.textura.height = btnJugar.rec.height = BOTON_TAM * 5 * config::escalado;
				
				btnCreditos.textura.width = btnCreditos.rec.width = BOTON_TAM * config::escalado;
				btnCreditos.textura.height = btnCreditos.rec.height = BOTON_TAM * config::escalado;
				
				btnMusica.textura.width = btnMusica.rec.width = BOTON_TAM * config::escalado;
				btnMusica.textura.height = btnMusica.rec.height = BOTON_TAM * config::escalado;

				btnEfectos.textura.width = btnEfectos.rec.width = BOTON_TAM * config::escalado;
				btnEfectos.textura.height = btnEfectos.rec.height = BOTON_TAM * config::escalado;

				btnSalir.textura.width = btnSalir.rec.width = BOTON_TAM * config::escalado;
				btnSalir.textura.height = btnSalir.rec.height = BOTON_TAM * config::escalado;

				// ----- Posicion de Botones -----
				btnJugar.rec.x = GetScreenWidth() / 2 - btnJugar.rec.width / 2;
				btnJugar.rec.y = GetScreenHeight() / 2 - btnJugar.rec.height / 2;
				
				if (marco::marcoActivo)
				{
					btnCreditos.rec.x = marco::marco.espaciado[static_cast <int>(marco::POSICION::IZQUIERDA)];
					btnCreditos.rec.y = marco::marco.espaciado[static_cast <int>(marco::POSICION::ARRIBA)];

					btnMusica.rec.x = marco::marco.espaciado[static_cast <int>(marco::POSICION::IZQUIERDA)];
					btnMusica.rec.y = GetScreenHeight() - btnMusica.rec.height - marco::marco.espaciado[static_cast <int>(marco::POSICION::ABAJO)];

					btnEfectos.rec.x = GetScreenWidth() - btnEfectos.rec.width - marco::marco.espaciado[static_cast <int>(marco::POSICION::DERECHA)];
					btnEfectos.rec.y = GetScreenHeight() - btnMusica.rec.height - marco::marco.espaciado[static_cast <int>(marco::POSICION::ABAJO)];

					btnSalir.rec.x = GetScreenWidth() / 2 - btnSalir.rec.width / 2;
					btnSalir.rec.y = GetScreenHeight() - marco::marco.espaciado[static_cast <int>(marco::POSICION::ABAJO)] / 2 - btnSalir.rec.height / 2;
				}
				else
				{
					btnOpciones.rec.x = GetScreenWidth() - btnOpciones.rec.width - OFFSET;
					btnOpciones.rec.y = OFFSET;

					btnCreditos.rec.x = OFFSET;
					btnCreditos.rec.y = OFFSET;

					btnMusica.rec.x = OFFSET;
					btnMusica.rec.y = GetScreenHeight() - btnMusica.rec.height - OFFSET;

					btnEfectos.rec.x = GetScreenWidth() - btnEfectos.rec.width - OFFSET;
					btnEfectos.rec.y = GetScreenHeight() - btnMusica.rec.height - OFFSET;

					btnSalir.rec.x = GetScreenWidth() / 2 - btnSalir.rec.width / 2;
					btnSalir.rec.y = GetScreenHeight() - btnMusica.rec.height - OFFSET;
				}
			}
			void update()
			{
				mouse = GetMousePosition();
				input();
				draw();
			}

			void input()
			{
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					if (CheckCollisionPointRec(mouse, btnJugar.rec))
					{
						juego::init();
						menuActual = MENU::JUEGO;
						audio::musica[0].activo = false;
						audio::musica[1].activo = true;
					}
					else if (CheckCollisionPointRec(mouse, btnOpciones.rec))
					{
						menuActual = MENU::OPCIONES;
						principal::deinit();
					}
					else if (CheckCollisionPointRec(mouse, btnCreditos.rec))
					{
						menuActual = MENU::CREDITOS;
					}
					else if (CheckCollisionPointRec(mouse, btnMusica.rec))
					{
						audio::musicas = !audio::musicas;
						actualizarTexturaAudio();
					}
					else if (CheckCollisionPointRec(mouse, btnEfectos.rec))
					{
						audio::efectos = !audio::efectos;
						actualizarTexturaAudio();
					}
					else if (CheckCollisionPointRec(mouse, btnSalir.rec))
					{
						config::enJuego = false;
					}
				}
			}
			void draw()
			{
				BeginDrawing();
				ClearBackground(WHITE);
				DrawTextureEx(fondo, { 0, 0 }, 0, 1, WHITE);
				
				marco::draw();
				DrawTextureEx(btnJugar.textura, { btnJugar.rec.x, btnJugar.rec.y }, 0, 1, btnJugar.color);
				DrawTextureEx(btnOpciones.textura, { btnOpciones.rec.x, btnOpciones.rec.y }, 0, 1, btnOpciones.color);
				DrawTextureEx(btnCreditos.textura, { btnCreditos.rec.x, btnCreditos.rec.y }, 0, 1, btnCreditos.color);
				DrawTextureEx(btnMusica.textura, { btnMusica.rec.x, btnMusica.rec.y }, 0, 1, btnMusica.color);
				DrawTextureEx(btnEfectos.textura, { btnEfectos.rec.x, btnEfectos.rec.y }, 0, 1, btnEfectos.color);
				DrawTextureEx(btnSalir.textura, { btnSalir.rec.x, btnSalir.rec.y }, 0, 1, btnSalir.color);

				EndDrawing();
			}
			void actualizarTexturaAudio()
			{
				if (audio::musicas)
				{
					UnloadTexture(btnMusica.textura);
					btnMusica.textura = LoadTexture("res/assets/menu/music_on.png");
				}
				else
				{
					UnloadTexture(btnMusica.textura);
					btnMusica.textura = LoadTexture("res/assets/menu/music_off.png");
				}

				if (audio::efectos)
				{
					UnloadTexture(btnEfectos.textura);
					btnEfectos.textura = LoadTexture("res/assets/menu/sound_on.png");
				}
				else
				{
					UnloadTexture(btnEfectos.textura);
					btnEfectos.textura = LoadTexture("res/assets/menu/sound_off.png");
				}

				btnMusica.textura.width = btnMusica.rec.width = BOTON_TAM * config::escalado;
				btnMusica.textura.height = btnMusica.rec.height = BOTON_TAM * config::escalado;

				btnEfectos.textura.width = btnEfectos.rec.width = BOTON_TAM * config::escalado;
				btnEfectos.textura.height = btnEfectos.rec.height = BOTON_TAM * config::escalado;
			}
			void deinit()
			{
				UnloadTexture(btnJugar.textura);
				UnloadTexture(btnOpciones.textura);
				UnloadTexture(btnCreditos.textura);
				UnloadTexture(btnMusica.textura);
				UnloadTexture(btnEfectos.textura);
				UnloadTexture(btnSalir.textura);
				UnloadTexture(fondo);
				marco::deinit();
			}
		}

		namespace creditos
		{
			void update()
			{
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					menuActual = MENU::PRINCIPAL;
				}
				if (IsKeyPressed(KEY_ONE) || IsKeyPressed(KEY_KP_1))
				{
					OpenURL("https://trello.com/b/MahUPDyz/dsvj12020olivematch3");
				}
				else if (IsKeyPressed(KEY_TWO) || IsKeyPressed(KEY_KP_2))
				{
					OpenURL("https://gitlab.com/federicoolive/dsvj1_2020_match_3");
				}
				draw();
			}
			void draw()
			{
				BeginDrawing();
				ClearBackground(WHITE);
				DrawTextureEx(fondo, { 0, 0 }, 0, 1, WHITE);

				DrawText("Programer: Federico Olive. - Estudiante de Image Campus, AR.", GetScreenWidth() / 2 - MeasureText("Programer: Federico Olive.", txt::tamFuente), 100, txt::tamFuente, RED);
				DrawText("Artist: Santiago Laffosse. - Estudiante de Image Campus, AR.", GetScreenWidth() / 2 - MeasureText("Artist: Santiago Laffosse.", txt::tamFuente), 150, txt::tamFuente, RED);

				DrawText("Presione 1 para abrir Trello.", GetScreenWidth() / 2 - MeasureText("Precione 1 para abrir Trello.", txt::tamFuente), 300, txt::tamFuente, BLUE);
				DrawText("Presione 2 para abrir GitLab.", GetScreenWidth() / 2 - MeasureText("Precione 2 para abrir GitLab", txt::tamFuente), 350, txt::tamFuente, BLUE);
				DrawText("Presione 3 para abrir referencias de asset.", GetScreenWidth() / 2 - MeasureText("Presione 3 para abrir refere", txt::tamFuente), 400, txt::tamFuente, BLUE);

				DrawText("Version 1.0", 0, GetScreenHeight() - 20, txt::tamFuente/2, RED);
				EndDrawing();
			}
		}

		namespace gameover
		{
			BOTON btnReiniciar;
			BOTON btnAlMenu;

			void init()
			{
				/*texturaBoton.width = 200 * config::escalado;
				texturaBoton.height = texturaBoton.width / 2;

				btnReiniciar.color = BLUE;
				btnAlMenu.color = PINK;

				btnReiniciar.textura = texturaBoton;
				btnAlMenu.textura = texturaBoton;

				btnReiniciar.rec.width = texturaBoton.width;
				btnAlMenu.rec.width = texturaBoton.width;

				btnReiniciar.rec.height = texturaBoton.height;
				btnAlMenu.rec.height = texturaBoton.height;

				btnReiniciar.rec.x = GetScreenWidth() / 2 - btnReiniciar.rec.width * 1.5f;
				btnAlMenu.rec.x = GetScreenWidth() / 2 + btnAlMenu.rec.width * 1.5f - btnAlMenu.rec.width;

				btnReiniciar.rec.y = GetScreenHeight() / 2 - btnReiniciar.rec.height / 2;
				btnAlMenu.rec.y = GetScreenHeight() / 2 - btnAlMenu.rec.height / 2;*/
			}
			void update()
			{
				mouse = GetMousePosition();
				input();
			}

			void input()
			{
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					if (CheckCollisionPointRec(mouse, btnReiniciar.rec))
					{
						//juego::init();
					}
					if (CheckCollisionPointRec(mouse, btnAlMenu.rec))
					{
						menuActual = MENU::PRINCIPAL;
					}
				}
			}

			void draw()
			{
				DrawTextureEx(btnReiniciar.textura, { btnReiniciar.rec.x, btnReiniciar.rec.y }, 0, 1, btnReiniciar.color);
				DrawTextureEx(btnAlMenu.textura, { btnAlMenu.rec.x, btnAlMenu.rec.y }, 0, 1, btnAlMenu.color);

				DrawText("Restart", btnReiniciar.rec.x + btnReiniciar.rec.width / 2 - MeasureText("Restart", txt::tamFuente) / 2, btnReiniciar.rec.y + btnReiniciar.rec.height / 2 - txt::tamFuente / 2, txt::tamFuente, WHITE);
				DrawText("Menu", btnAlMenu.rec.x + btnAlMenu.rec.width / 2 - MeasureText("Menu", txt::tamFuente) / 2, btnAlMenu.rec.y + btnAlMenu.rec.height / 2 - txt::tamFuente / 2, txt::tamFuente, WHITE);

			}
		}
	}
}




