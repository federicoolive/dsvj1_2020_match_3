#ifndef MENUES_H
#define MENUES_H

#include "raylib.h"
#define BOTON_TAM 50

namespace match
{
	namespace menues
	{
		enum class MENU { PRINCIPAL, JUEGO, OPCIONES, CREDITOS, LIMITE };
		extern MENU menuActual;
		extern int menuInmersion;

		void init();

		namespace principal
		{
			void init();
			void update();
			void input();
			void draw();
			void actualizarTexturaAudio();
			void deinit();
		}

		namespace creditos
		{
			void update();
			void draw();
		}

		namespace gameover
		{
			void init();
			void update();
			void input();
			void draw();
		}
	}
}




#endif