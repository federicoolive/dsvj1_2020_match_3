#include "audio.h"

namespace match
{
	namespace audio
	{
		MUSICA musica[static_cast<int>(TIPOMUSICA::LIMITE)];
		EFECTOS efecto[static_cast<int>(TIPOEFECTO::LIMITE)];

		bool musicas = true;
		bool efectos = true;

		void init()
		{
			musica[static_cast<int>(TIPOMUSICA::MENU)].musica = LoadMusicStream("res/assets/sonido/menu.mp3");
			musica[static_cast<int>(TIPOMUSICA::JUEGO)].musica = LoadMusicStream("res/assets/sonido/juego.mp3");
			musica[static_cast<int>(TIPOMUSICA::JUEGO2)].musica = LoadMusicStream("res/assets/sonido/juego2.mp3");
			musica[static_cast<int>(TIPOMUSICA::JUEGO3)].musica;

			for (int i = 0; i < static_cast<int>(TIPOMUSICA::LIMITE); i++)
			{
				PlayMusicStream(musica[i].musica);
			}

			efecto[static_cast<int>(TIPOEFECTO::MATCH)].efecto = LoadSound("res/assets/sonido/match3.mp3");
			efecto[static_cast<int>(TIPOEFECTO::ATAQUE)].efecto;
			efecto[static_cast<int>(TIPOEFECTO::GAMEOVER)].efecto;
		}

		void update()
		{
			if (musicas)
			{
				for (int i = 0; i < static_cast<int>(TIPOMUSICA::LIMITE); i++)
				{
					if (musica[i].activo)
					{
						UpdateMusicStream(musica[i].musica);
					}
				}
			}

			if (efectos)
			{
				for (int i = 0; i < static_cast<int>(TIPOEFECTO::LIMITE); i++)
				{
					if (efecto[i].activo)
					{
						efecto[i].activo = false;
						PlaySound(efecto[i].efecto);
					}
				}
			}
		}

		void deinit()
		{
			for (int i = 0; i < static_cast<int>(TIPOMUSICA::LIMITE); i++)
			{
				UnloadMusicStream(musica[i].musica);
			}

			for (int i = 0; i < static_cast<int>(TIPOEFECTO::LIMITE); i++)
			{
				UnloadSound(efecto[i].efecto);
			}
		}
	}
}