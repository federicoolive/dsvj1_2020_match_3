#ifndef AUDIO_H
#define AUDIO_H

#include "raylib.h"

namespace match
{
	namespace audio
	{
		enum class TIPOMUSICA { MENU, JUEGO, JUEGO2, JUEGO3, LIMITE };
		struct MUSICA
		{
			bool activo = false;
			Music musica;
		};
		extern MUSICA musica[static_cast<int>(TIPOMUSICA::LIMITE)];

		enum class TIPOEFECTO { MATCH, ATAQUE, GAMEOVER, LIMITE };
		struct EFECTOS
		{
			bool activo = false;
			Sound efecto;
		};
		extern EFECTOS efecto[static_cast<int>(TIPOEFECTO::LIMITE)];

		extern bool musicas;
		extern bool efectos;

		void init();
		void update();
		void deinit();
	}
}

#endif