#ifndef ANIMACIONES_H
#define ANIMACIONES_H

#include "raylib.h"
#include <time.h>
#include "juego/runas.h"
#include "juego/juego.h"


namespace match
{
	namespace animaciones
	{
		extern int clockAnim;
		enum class ESTADOANIM { NINGUNO, DESTRUCCION, ORDENAMIENTO, APARICION };
		extern ESTADOANIM estadoAnim;

		void animacionesManagerRunas();
		void animacionesManagerPersonaje();

	}
}

#endif