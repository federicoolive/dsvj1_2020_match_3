#include "animaciones.h"
#include "origen/loop.h"
#include "jugador/jugador.h"
#include "audio/audio.h"

namespace match
{
	namespace animaciones
	{
		int clockAnim = 0;
		ESTADOANIM estadoAnim = ESTADOANIM::NINGUNO;
		void animacionesManagerRunas()
		{
			switch (estadoAnim)
			{
			case animaciones::ESTADOANIM::NINGUNO:
				if (IsMouseButtonUp(MOUSE_LEFT_BUTTON) && juego::ultimoSeleccionado >= constantes::minCombo - 1)
				{
					audio::efecto [static_cast<int>(audio::TIPOEFECTO::MATCH)].activo = true;
					estadoAnim = ESTADOANIM::DESTRUCCION;
					juego::score += juego::ultimoSeleccionado * 3 * juego::ultimoSeleccionado;
					juego::turnosActuales++;
					clockAnim = clock();
					for (int i = 0; i < tamCol; i++)
					{
						for (int j = 0; j < tamFil; j++)
						{
							runas::bloque[i][j].stat.color = WHITE;
							runas::bloque[i][j].enSeleccion = false;
						}
					}
				}
				break;
			case animaciones::ESTADOANIM::DESTRUCCION:
				if (loop::clockUpdate > clockAnim + tiempoDestruccion)
				{
					clockAnim = clock();
					runas::destruccion();
				}

				break;
			case animaciones::ESTADOANIM::ORDENAMIENTO:
				runas::bajarRunas();
				
				break;
			case animaciones::ESTADOANIM::APARICION:
				if (loop::clockUpdate > clockAnim + tiempoAparicion)
				{
					clockAnim = clock();
					runas::reaparecerBloques();
				}

				break;
			default:
				break;
			}
		}

		void animacionesManagerPersonaje()
		{
			/*switch (jugadores::jugador.estado)
			{
			case jugadores::ESTADO::DESCANSANDO:

				if (juego::hayAtaque)
				{
					jugadores::jugador.estado = jugadores::ESTADO::ATACANDO;
					jugadores::clockAnim = clock();
				}

				break;
			case jugadores::ESTADO::ATACANDO:

				if (loop::clockUpdate > jugadores::clockAnim + tiempoDestruccion)
				{
					jugadores::ataque();
				}

				break;
			case jugadores::ESTADO::DEFENDIENDO:



				break;
			default:
				break;
			}

			switch (jugadores::enemigo.estado)
			{
			case jugadores::ESTADO::DESCANSANDO:



				break;
			case jugadores::ESTADO::ATACANDO:



				break;
			case jugadores::ESTADO::DEFENDIENDO:



				break;
			default:
				break;
			}*/



		}
	}
}