#ifndef MARCO_H
#define MARCO_H

#include "raylib.h"
#define OFFSET 15
#define LAT_MARCO 10
#define ARR_MARCO 45
#define ABJ_MARCO 57

namespace match
{
	namespace marco
	{
		const bool marcoActivo = false;
		enum class POSICION { DERECHA, IZQUIERDA, ARRIBA, ABAJO, LIMITE };
		
		struct MARCO
		{
			Texture2D textura;
			int espaciado[static_cast<int>(POSICION::LIMITE)];
		};
		extern MARCO marco;
		void init();
		void draw();
		void deinit();
	}
}

#endif