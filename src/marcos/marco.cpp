#include "marco.h"
#include "configuraciones/configuraciones.h"

namespace match
{
	namespace marco
	{
		MARCO marco;

		void init()
		{
			marco.textura = LoadTexture("res/assets/texturas/Marco_Celular.png");
			marco.textura.width = GetScreenWidth();
			marco.textura.height = GetScreenHeight();
			marco.espaciado[static_cast<int>(POSICION::DERECHA)] = (LAT_MARCO + OFFSET) * config::escalado;
			marco.espaciado[static_cast<int>(POSICION::IZQUIERDA)] = (LAT_MARCO + OFFSET) * config::escalado;
			marco.espaciado[static_cast<int>(POSICION::ARRIBA)] = (ARR_MARCO + OFFSET) * config::escalado;
			marco.espaciado[static_cast<int>(POSICION::ABAJO)] = (ABJ_MARCO + OFFSET) * config::escalado;
		}

		void draw()
		{
			if (marcoActivo)
				DrawTextureEx(marco.textura, { 0,0 }, 0, 1, WHITE);
		}

		void deinit()
		{
			UnloadTexture(marco.textura);
		}
	}
}