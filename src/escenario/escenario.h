#ifndef ESCENARIO_H
#define ESCENARIO_H

#include "raylib.h"

namespace match
{
	namespace escenario
	{
		extern Rectangle panelMatch;
		extern Rectangle panelPausa;
		extern Texture2D fondo;
		void init();
		void update();
		void draw();
		void deinit();
	}
}

#endif