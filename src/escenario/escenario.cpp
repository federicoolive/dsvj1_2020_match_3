#include "escenario.h"
#include <iostream>
#include "juego/juego.h"
#include "juego/runas.h"
#include "configuraciones/configuraciones.h"

using namespace std;

namespace match
{
	namespace escenario
	{
		Texture2D fondo;
		Rectangle panelMatch;
		Rectangle panelPausa;

		void init()
		{
			panelMatch.width = tamCol * ancho * config::escalado;
			panelMatch.height = tamFil * alto * config::escalado;
			panelMatch.x = GetScreenWidth() / 2 - panelMatch.width / 2;
			panelMatch.y = GetScreenHeight() * 3 / 10;

			panelPausa.width = GetScreenWidth() * 5 / 10 * config::escalado;
			panelPausa.height = GetScreenHeight() * 4 / 10 * config::escalado;
			panelPausa.x = GetScreenWidth() / 2 - panelPausa.width / 2;
			panelPausa.y = GetScreenHeight() / 2 - panelPausa.height;

			fondo = LoadTexture("res/assets/menu/bg.png");
			fondo.width = GetScreenWidth();
			fondo.height = GetScreenHeight();
		}

		void update()
		{
			
		}
		void draw()
		{
			DrawTextureEx(escenario::fondo, { 0, 0 }, 0, 1, WHITE);
			DrawRectangleRec(panelMatch, GRAY);
		}
		void deinit()
		{

		}
	}
}